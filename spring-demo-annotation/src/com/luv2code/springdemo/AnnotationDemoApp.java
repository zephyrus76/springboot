package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationDemoApp {

	public static void main(String[] args) {
	//read spring config file
ClassPathXmlApplicationContext context=
new ClassPathXmlApplicationContext("applicationContext.xml");		
	
		//get the bean from spring container
//	Coach coach=context.getBean("tensadnisCoach",Coach.class); expicit bean id
Coach coach=context.getBean("tennisCoach",Coach.class); //@Componet generated bean id class name with first letter of the classname as small	
Coach coach1=context.getBean("tennisCoach",Coach.class);
//call a method on the bean


System.out.println("coach"+coach);	

System.out.println("coach1"+coach1);

	
	System.out.println(coach.getDailyWorkout());	
		
	System.out.println(coach.getDailyFortune());	
		
		//close the context
	context.close();	
		
	}

}
