package com.luv2code.springdemo;

//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component("tensadnisCoach") not default component id name

@Component
//@Scope("prototype")// for sinleton scope @Scope("Singleton")
public class TennisCoach implements Coach {
	
	//@Autowired //just work fine like constructor and//setter injection by using java reflection technology
	//@Qualifier("HANDLERfs")//@Qualifier("DBfs")//@Qualifier("RESTfs")
	private FortuneService fs ;
	
//	@Autowired //after commenting also worked due the the feature of spring 4.3 
//	TennisCoach(@Qualifier("HANDLERfs")
//	FortuneService fs)
//	{
//	System.out.println("constructor");
//	this.fs=fs;	
//	}

	TennisCoach()
	{
	}
	
	
     @Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "practice your backend volley";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fs.getFortune();
	}

 //can set the autowired or dependency injection on any method
//@Qualifier("HANDLERfs")
///this way of doing it is been used if there is multiple implemtation to the interface by using 	
@Autowired	
public void setFortuneService(@Qualifier("DBfs") FortuneService fs)
{
	System.out.println("setter method");
this.fs=fs;	
	
}


//init method
//@PostConstruct //after the constructor
public void doMyStartupStuff()
{
	
System.out.println("init method");	
}

//@PreDestroy
public void doMyDestroyStuff()
{
	System.out.println("destroy method");
	
}



}
