package javaEightFeatures;
import java.util.function.*;
public class Functionz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Predicate<Integer> p=I->I%2==0;
Predicate<Integer> q=I->I%2!=0;
	
	System.out.println(p.negate().test(100));
	System.out.println(p.and(q).test(100));
	System.out.println(p.or(q).test(100));

Consumer<Integer> c=(I)->System.out.println("the value of integer is "+I);	
Consumer<Integer> d=(I)->System.out.println("the value of integer2 is "+I);	
	
if(p.test(1001))c.accept(1001);
else d.accept(1001);	
	
Supplier<String> s=()-> {

return "fruit";
};	

	
System.out.println(s.get());	
	

Function<Integer,Integer> f=(I)->I+I;

Function<Integer,Integer> f1=(I)->I*I;

System.out.println(f.andThen(f1).apply(1));
System.out.println(f.compose(f1).apply(1));

//Premitive Function


IntPredicate even=x-> x%2==0;
DoublePredicate evend=x-> x%2==0;
LongPredicate evenl=x-> x%2==0;
	
IntConsumer	a=null;
/// same

IntSupplier sup;
//same
	//TWO ARGUMENT BiPredicates
BiPredicate<Integer,Integer> bp=(value1,value2)->(value1+value2)%2==0;

System.out.println(bp.test(13,32));

	
BiFunction<Integer,Integer,Integer> bf=(value3,value4)->value3+value4;
	
System.out.println(bf.apply(12,24));	
	

BiConsumer<Integer,Integer> z=(s1,s2)->System.out.println(s1+s2);
z.accept(12, 23);
	}

}
