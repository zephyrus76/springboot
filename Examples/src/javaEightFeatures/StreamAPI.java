package javaEightFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class StreamAPI {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*	
List<Integer> lst=new ArrayList<>();	
	lst.add(1203);
	lst.add(1203);
	lst.add(110);
	lst.add(3102);
	lst.add(2101);
	lst.add(11101);
	lst.add(5107);
	lst.add(610);
	lst.add(3107);
	lst.add(210);
	lst.add(7108);
	lst.add(5103);
	lst.add(8103);
	lst.add(-105);
	System.out.println("1st original::::::::>"+lst);
	lst.parallelStream().map((i)->i*1).filter((i)->i/1==i).sorted().forEach(System.out::println);
	//lst.stream().map((i)->i*1).filter((i)->i/1==i).distinct().sorted().forEach(System.out::println);
	
	//System.out.println("2st filtered::::::::>"+pq);
*/	
dwao dw=new dwao(23,"a");

	
		List<dwao> d=new ArrayList<>();	
	d.add(dw);
	d.add(new dwao(2,"ab"));
	d.add(new dwao(12,"xb"));
	d.add(new dwao(3,"zb"));
	d.add(new dwao(121,"zb"));
	
	d.add(new dwao(2,"ab"));
	d.add(new dwao(4,"db"));
	d.add(new dwao(3,"qb"));
	List<Integer> dp=d.parallelStream().map(dwao::getId).distinct().limit(12).sequential().collect(Collectors.toList());
	System.out.println("parallelStream getId distinct limit(12) seqential toList");
	System.out.println(dp);
	
//lazy or transformation
// no printing is happened	d.stream().filter(e->e.getId()%2==0);///

	
	//action
//	d.stream().filter(e->e.getId()%2==0).map(dwao::getId).forEach(System.out::println);;
	System.out.println("filter divide by 1 map to getId reduce to sum");
	System.out.println(d.stream().filter(e->e.getId()/1==e.getId()).map(dwao::getId).reduce(0,(c,e)->c+e,Integer::sum));
	System.out.println("filter divide by 1 mapToInt to getId reduce to sum");
	System.out.println(d.stream().filter(e->e.getId()/1==e.getId()).mapToInt(dwao::getId).reduce(0,(c,e)->c+e));
	System.out.println("filter divide by 1 map to getNamae map to toUpperCase skip 3  forEach println");
d.stream().filter(e->e.getId()/1==e.getId()).map(dwao::getName).map(String::toUpperCase).skip(3).forEach(System.out::println);
	
//::::::>>>>> Grouping
System.out.println("::::::>>>>> Grouping by using dwao::getId");
	Map<Integer, List<dwao>> lsd=d.parallelStream().collect(Collectors.groupingBy(dwao::getId));
	System.out.println(lsd);
	
	System.out.println("::::::>>>>> Grouping by using dwao::getId then by dwao::getName");
Map<Integer, Map<String, List<dwao>>> lsg=d.stream().collect(Collectors.groupingBy((dwao::getId),Collectors.groupingBy(dwao::getName)));
System.out.println(lsg);

	//
	/*Map<Boolean,List<dwao>> heroine=d.stream().collect(Collectors.partitioningBy(e->e.id%2==0));
	System.out.println(heroine);*/

//let do it by using groupby
//Map<Boolean,List<dwao>> heroine=d.stream().collect(Collectors.partitioningBy(e->e.id%2==0));
//System.out.println(heroine);	
	//flatmap

//would work if getname takes a list of values rather than single value 

//d.stream().flatMap(e->e.getName().stream()).forEach(System.out::println);
//System.out.println(d.parallelStream().mapToInt(dwao::getId).min());

//serial Execution	
OptionalInt z=d.stream().mapToInt(t->{System.out.println("Id is "+t.getId()+"Thread is "+Thread.currentThread().getName());
return t.getId();
}).reduce(Integer::sum);
System.out.println(z);	


//parallel Execution	
OptionalInt x=d.parallelStream().mapToInt(t->{System.out.println("Id is "+t.getId()+"Thread is "+Thread.currentThread().getName());
return t.getId();
}).reduce(Integer::sum);


	System.out.println(x);
	
	ArrayList<String> lk = new ArrayList<String>();
	 lk.add("rvk");
	 lk.add("rk");
	 lk.add("rkv"); 
	 lk.add("rvki"); 
	 lk.add("rvkir");
	 System.out.println(lk);
	 List<String> l2 = lk.stream().map(s ->s.toUpperCase()).collect(Collectors.toList());
	 System.out.println(l2);
	
	}

}
