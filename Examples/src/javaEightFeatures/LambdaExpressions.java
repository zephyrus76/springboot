package javaEightFeatures;

public class LambdaExpressions {
interface Greeting{
	public String sayhello(String g);
}

public void testGreeting(String a,Greeting g)
{
	System.out.println("result  "+g.sayhello(a));
	
}


public static void main(String args[])
{
	new LambdaExpressions().testGreeting("Sanjiv Rai",(p)->"Hello "+p);
	
}
}
