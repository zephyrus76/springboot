package javaEightFeatures;
interface Engine{
	default String make() {
		return "Default Make";
	}
	
	
}
//interface Vehicle{
interface Vehicle extends Engine{
	default String make() {
		return "Default Model";
	}
	void hello();
	
	
}
//In such scenario where two same default methods please implementation of 1 default method
/* class I implements Vehicle,Engine
 {
	 String m1()
	 {
		 //return this.make()+Vehicle.super.make();
		 return Vehicle.super.make();
	 }

	//@Override
	 /*public String make() {
		// TODO Auto-generated method stub
		return Engine.super.make();
	}*/
 //}
public class InheritingBehaviour {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*I i=new I();
System.out.println(i.m1());
*/
		
		
	Vehicle v=()->System.out.println("Hello");
		
	}

}
