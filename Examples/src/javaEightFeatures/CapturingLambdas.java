package javaEightFeatures;
/*never mutate static ,instance and local variable in lambda expression*/
public class CapturingLambdas {
interface Interf{
	public int cnt(int c);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

//Interf i=p->p*2; cannot refer the outer value becomes static final



		CapturingLambdas q=new CapturingLambdas();
		q.cont(23);
System.out.println(q.p);
	}
	int p=2;

public void cont(int w)
{
	int q=23;
	//Interf i=p->w++; //w is rfective final cant incrememt it
	//Interf i=p->q++; //q is also effective final
	Interf i=p->++p;
	
	System.out.println(i.cnt(p)+"effective value"+p);
}
}
