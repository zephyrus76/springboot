package javaEightFeatures;
public class dwao
{
	int id;
	String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public dwao(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public dwao() {
	}
	@Override
	public String toString() {
		return "dwao [id=" + id + ", name=" + name + "]";
	}
	
}