package com.rajat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



public class treeSetexp 
{
	 void merge(int arr[], int l, int m, int r)
	    {
	        // Find sizes of two subarrays to be merged
	        int n1 = m - l + 1;
	        int n2 = r - m;
	 
	        /* Create temp arrays */
	        int L[] = new int[n1];
	        int R[] = new int[n2];
	 
	        /*Copy data to temp arrays*/
	        for (int i = 0; i < n1; ++i)
	            L[i] = arr[l + i];
	        for (int j = 0; j < n2; ++j)
	            R[j] = arr[m + 1 + j];
	 
	        /* Merge the temp arrays */
	 
	        // Initial indexes of first and second subarrays
	        int i = 0, j = 0;
	 
	        // Initial index of merged subarry array
	        int k = l;
	        while (i < n1 && j < n2) {
	            if (L[i] <= R[j]) {
	                arr[k] = L[i];
	                i++;
	            }
	            else {
	                arr[k] = R[j];
	                j++;
	            }
	            k++;
	        }
	 
	        /* Copy remaining elements of L[] if any */
	        while (i < n1) {
	            arr[k] = L[i];
	            i++;
	            k++;
	        }
	 
	        /* Copy remaining elements of R[] if any */
	        while (j < n2) {
	            arr[k] = R[j];
	            j++;
	            k++;
	        }
	    }
	public void mergesort(int arr[],int l,int h)
	{
		int mid=0;
	 if(l<h)
	 {
		 mid=(l+h)/2;
	 
		mergesort(arr,l,mid);
		mergesort(arr,mid+1,h);
		
		merge(arr,l,mid,h);
	 }	
	}
	
	
	
	
	
    public static void main(String[] args) 
    {
    	
    int a[]= {12,2,3,1,7,3,13};	
    treeSetexp m=new treeSetexp();	
    m.mergesort(a,0,a.length-1);
    
    for(int p:a)
    {
    	System.out.println(p);
    }
    
    Custom c1 = new Custom(6,"A");
    Custom c2 = new Custom(3,"C");
    Custom c3 = new Custom(4,"B");

  /*  TreeMap<Custom , Integer > tree = new TreeMap<Custom, Integer>  (new Comparator<Custom>() {

                                            @Override
                                            public int compare(Custom o1, Custom o2) {

                                                return -o1.name.compareTo(o2.name);
                                            }
                                        });
    tree.put(c1, 1);
    tree.put(c2, 2);
    tree.put(c3, 3);
*/

ArrayList<Custom> al=new ArrayList<>();
al.add(c1);
al.add(c2);
al.add(c3);
al.add(new Custom(1,"Z"));




for(Custom p:al)
		{
	System.out.println(p.rank);
		}
//System.out.println(al.stream().filter(e->e.rank%2==0).collect(Collectors.toList()));
int value=al.stream().filter(Custom::div).reduce(1,(c,e)->{
	return c*e.rank;	
},Integer::sum);
System.out.println(value);

al.parallelStream().map(e->{
	e.rank=e.rank*2;
	e.name=e.name+" sanjiv";
	return e;
}).collect(Collectors.toList());
System.out.println(al);
System.out.println(al.stream().max((c,d)->{
	int cq=c.rank;
	int cw=d.rank;
	return (cq<cw)?-1:(cq>cw)?1:0;
}));
/*List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
int result = numbers
  .stream().filter(null)
  .reduce(0, (subtotal, element) -> subtotal + element);
*/
//  System.out.println(tree);

    

    
    
    
    
    
    
    
    
    
    
    
    
    
    }
}

class Custom
{
int rank ; 
String name ; 
public Custom(int rank , String name) {
    this.rank = rank ;
    this.name = name ;
}

@Override
public String toString()
{
    return "Custom[" + this.rank + "-" + this.name + "]" ;
}

public static boolean div(Custom e)
{
	return e.rank%2==0;
}
}