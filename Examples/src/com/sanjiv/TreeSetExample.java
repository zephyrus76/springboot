package com.sanjiv;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
 class Deco
{
	int id;
	String fname;
	String lname;
	public Deco(int id, String fname, String lname) {
		this.id = id;
		this.fname = fname;
		this.lname = lname;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	@Override
	public String toString() {
		return "Deco [id=" + id + ", fname=" + fname + ", lname=" + lname + "]";
	}

	
}

public class TreeSetExample {

	public static void main(String[] args)
{	Set<Deco> s=new TreeSet<Deco>(new comp());

s.add(new Deco(1,"sanjiv","rai"));
s.add(new Deco(7,"zqwqe","ppp"));
s.add(new Deco(3,"qwert","dadd"));

System.out.println(s);

}}
