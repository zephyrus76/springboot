package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Aspect
@Component
public class MyDemoLoggingAspect {

	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.*(..))")
private void forDaoPackage() {}


@Before("forDaoPackage()")//all methods of all classes in dao package
	public void beforeAddAccountAdvice() 
	{
	System.out.println("=========> executing before advice Dao ");	
	}
/*
@After("forDaoPackage()")
public void performApiAnalytics() {
	System.out.println("=========> executing after analytics Dao ");	
}*/
//pointcut for getters method
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.get*(..))")
	private void forGetDaoPackage() {}

//pointcut for setter method

	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.set*(..))")
	private void forSetDaoPackage() {}

	@Pointcut("forDaoPackage() && !(forGetDaoPackage() || forSetDaoPackage())")
	private void forNotGetSetDaoPackage() {}
	
	
	@After("forNotGetSetDaoPackage()")
	public void performnotGetandSet() {
		System.out.println("=========> executing after not getter and setter Dao ");	
	}
	
	
	
	
	@Before("forGetDaoPackage()")
	private void gett() {
		System.out.println("getter method");
	}
	
	@Before("forSetDaoPackage()")
	private void sett() {
		System.out.println("getter method");
	}
	
	
	
	
	//create pointcut : include package .. exclude getter/setter
	

}





