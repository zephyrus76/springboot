package com.luv2code.aopdemo.dao;

import org.springframework.stereotype.Component;

import com.luv2code.aopdemo.Account;

@Component
public class AccountDao {

	private String name;
	private String serviceCode;
	
public void addAccount(Account theAccount,boolean vipFlag)
{
theAccount.setLevel("level 1");
	
	System.out.println(theAccount+"DOING ACCOUNT DAO WORK");
}

public String getName() {

	System.out.println("getname");
	return name;
}

public void setName(String name) {

	System.out.println("setname");
	this.name = name;
}

public String getServiceCode() {
	System.out.println("getservicecode");
	return serviceCode;
}

public void setServiceCode(String serviceCode) {

	System.out.println("getservicecode");
	this.serviceCode = serviceCode;
	
}

}
