package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	//create Session Factory
SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
	//create session
	Session session=factory.getCurrentSession();	
		
	try
	{
		//use the session object to save Java object
		System.out.println("create new student object...");
	//Student tempStudent=new Student("Paul","Hey","paulH@luv2code.com");
		Student tempStudent=new Student("pam","baesely","pb@luv2code.com");
		/*Student tempStudent1=new Student("xc","xv","fsfs@luv2code.com");
		Student tempStudent2=new Student("qwerty","qwer","qwert@luv2code.com");
	*/	
	//start the session
		
		System.out.println("id before adding "+tempStudent.getId());
		
	session.beginTransaction();
	
	//save the student obj
	System.out.println("saving the student object");
	
	session.save(tempStudent);
//	session.save(tempStudent1);
//	session.save(tempStudent2);
//	
	System.out.println("id after adding "+tempStudent.getId());
	
	//commit the change
	session.getTransaction().commit();

	
	System.out.println("Done !");
	}
	catch(Exception e)
	{
	e.printStackTrace();	
	}
	finally
	{
		factory.close();
	}
	
		
	}

}
