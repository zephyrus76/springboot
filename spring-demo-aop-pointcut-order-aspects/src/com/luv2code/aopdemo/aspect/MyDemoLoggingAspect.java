package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Aspect
@Component
public class MyDemoLoggingAspect {



	
	
	@Before("forGetDaoPackage()")
	private void gett() {
		System.out.println("getter method");
	}
	
	@Before("forSetDaoPackage()")
	private void sett() {
		System.out.println("setter method");
	}
	
	
	
	
	//create pointcut : include package .. exclude getter/setter
	

}





