package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {
	
	
	

	@Before("forDaoPackage()")//all methods of all classes in dao package
	public void beforeAddAccountAdvice() 
	{
	System.out.println("=========> executing before advice Dao ");	
	}
	
	@After("forNotGetSetDaoPackage()")
	public void performnotGetandSet() {
		System.out.println("=========> executing after not getter and setter Dao ");	
	}
	
	
}
