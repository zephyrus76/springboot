package com.luv2code.aopdemo.aspect;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyCloudLog {
	@After("forNotGetSetDaoPackage()")
	public void logToCloudAsynch() {
		System.out.println("=========> logging to cloud");	
	}
}
