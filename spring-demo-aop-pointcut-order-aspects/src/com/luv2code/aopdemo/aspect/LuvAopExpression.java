package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LuvAopExpression {
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.*(..))")
	public void forDaoPackage() {}


	
	/*
	@After("forDaoPackage()")
	public void performApiAnalytics() {
		System.out.println("=========> executing after analytics Dao ");	
	}*/
	//pointcut for getters method
		@Pointcut("execution(* com.luv2code.aopdemo.dao.*.get*(..))")
		public void forGetDaoPackage() {}

	//pointcut for setter method

		@Pointcut("execution(* com.luv2code.aopdemo.dao.*.set*(..))")
		public void forSetDaoPackage() {}

		@Pointcut("forDaoPackage() && !(forGetDaoPackage() || forSetDaoPackage())")
		public void forNotGetSetDaoPackage() {}
		
		
}
