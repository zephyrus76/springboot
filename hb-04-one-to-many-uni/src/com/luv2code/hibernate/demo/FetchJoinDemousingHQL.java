package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class FetchJoinDemousingHQL {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	//create Session Factory
SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").
addAnnotatedClass(Instructor.class).
addAnnotatedClass(InstructorDetail.class).
addAnnotatedClass(Course.class).
buildSessionFactory();
		
	//create session
	Session session=factory.getCurrentSession();	
		
	try
	{

	session.beginTransaction();
	//get the id
	int theid=1;
	
	
//option 2 using HQL
	
	//GET iNSTRUCTOR
	
	Query<Instructor> query=session.createQuery("select i from Instructor i "
			+ "JOIN FETCH i.courses "
			+ "where i.id=:theInstructorId",Instructor.class);
	query.setParameter("theInstructorId",1);
	
///execute the quer\
	Instructor tempInstructor=query.getSingleResult();
	
	System.out.println("Instructor"+tempInstructor);
	
	//commit transaction
	session.getTransaction().commit();
	
	
	session.close();
	//can use it even after the session is close 
	
	System.out.println("Courses"+tempInstructor.getCourses());
	
	System.out.println("Done !");
	}
	catch(Exception e)
	{
	e.printStackTrace();	
	}
	finally
	{
		factory.close();
	}
	
		
	}

}
