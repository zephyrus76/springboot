package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class CreateCoursesDemo {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	//create Session Factory
SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").
addAnnotatedClass(Instructor.class).
addAnnotatedClass(InstructorDetail.class).
addAnnotatedClass(Course.class).
buildSessionFactory();
		
	//create session
	Session session=factory.getCurrentSession();	
		
	try
	{

	session.beginTransaction();
	//get the id
	int theid=2;
	
	//get the instructor
	Instructor tempInstructor=session.get(Instructor.class,theid);
	
	Course t1=new Course("Air  - The Ultimate Guide");
	Course t2=new Course("The  ball MasterClass");
	
	tempInstructor.add(t1);
	tempInstructor.add(t2);
	
	session.save(t1);
	session.save(t2);
	
	session.getTransaction().commit();

	
	System.out.println("Done !");
	}
	catch(Exception e)
	{
	e.printStackTrace();	
	}
	finally
	{
		factory.close();
	}
	
		
	}

}
