package com.luv2code.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.luv2code.aopdemo.dao.AccountDao;
import com.luv2code.aopdemo.dao.DemoConfig;
import com.luv2code.aopdemo.dao.MembershipDAO;

public class MainDemoApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
AnnotationConfigApplicationContext context=new 
AnnotationConfigApplicationContext(DemoConfig.class);
		
	AccountDao dao=context.getBean("accountDao",AccountDao.class);
		
	dao.addAccount(new com.luv2code.aopdemo.Account(),true);
	
	MembershipDAO dao1=context.getBean("membershipDAO",MembershipDAO.class);
	
	
	dao1.addSilly();
	context.close();
	}

}
