package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
@Aspect
@Component
public class MyDemoLoggingAspect {

	
	
//	@Before("execution(public void add*())") //pointcut expression: A predicate expression for where advice should be applied
	//@Before("execution(void add*())")// modifier is optional
//	@Before("execution(* *())")
	//@Before("execution(* *(com.luv2code.aopdemo.Account))") //use fully qualified name only otherwise it might break
	//@Before("execution(* *(com.luv2code.aopdemo.Account, ..))")
//	@Before("execution(* *(..))")//any nuber of param
	
	
	@Before("execution(* com.luv2code.aopdemo.dao.*.*(..))")//all methods of all classes in dao package
	public void beforeAddAccountAdvice() 
	{
	System.out.println("=========> executing before advice Dao ");	
	}
}
//exection(modifier-pattern?return type-pattern declaring-type-pattern?
//method-name-pattern(param-pattern) throws-pattern?)

//declaring type-pattern class name
//method-name-pattern method name to match
//Modifier: Spring AOP Only support public or *
//param-pattern method parameter
//throws-pattern throws exception


/*for param-pattern
* () - matches a method with no arguments
* (*) -matches a method with one argument of any types
* (..) -matches a method with0 or more arguments of any types
*
*
*
*
*
*
*/