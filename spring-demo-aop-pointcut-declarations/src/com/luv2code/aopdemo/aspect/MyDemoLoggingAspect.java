package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Aspect
@Component
public class MyDemoLoggingAspect {

	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.*(..))")
private void forDaoPackage() {}


@Before("forDaoPackage()")//all methods of all classes in dao package
	public void beforeAddAccountAdvice() 
	{
	System.out.println("=========> executing before advice Dao ");	
	}

@After("forDaoPackage()")
public void performApiAnalytics() {
	System.out.println("=========> executing after analytics Dao ");	
	
}


}





