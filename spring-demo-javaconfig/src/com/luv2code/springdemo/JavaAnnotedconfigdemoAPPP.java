package com.luv2code.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JavaAnnotedconfigdemoAPPP {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AnnotationConfigApplicationContext context=
				new AnnotationConfigApplicationContext(SportsConfig.class);		
		SwimCoach ch=context.getBean("swimCoach",SwimCoach.class);
		
		System.out.println(ch.getDailyFortune());
		System.out.println(ch.getDailyWorkout());
		System.out.println(ch.getEmail());
		System.out.println(ch.getTeam());
		context.close();
	}

}
