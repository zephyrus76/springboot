package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;

public class DeleteCourseandReviewDemo {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	//create Session Factory
SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").
addAnnotatedClass(Instructor.class).
addAnnotatedClass(InstructorDetail.class).
addAnnotatedClass(Course.class).
addAnnotatedClass(Review.class).buildSessionFactory();
		
	//create session
	Session session=factory.getCurrentSession();	
		
	try
	{

	session.beginTransaction();
	//get the id
	int theid=10;
	Course tempCourse=session.get(Course.class,theid);
	
	System.out.println("Course to delete: "+tempCourse);
	
	//
	System.out.println("the reviews  to delete"+tempCourse.getReviews());
	
	
	session.delete(tempCourse);
	session.getTransaction().commit();
	
	

	
	System.out.println("Done !");
	}
	catch(Exception e)
	{
	e.printStackTrace();	
	}
	finally
	{
		factory.close();
	}
	
		
	}

}
