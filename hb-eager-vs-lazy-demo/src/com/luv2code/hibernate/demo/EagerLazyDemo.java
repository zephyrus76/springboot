package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	//create Session Factory
SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").
addAnnotatedClass(Instructor.class).
addAnnotatedClass(InstructorDetail.class).
addAnnotatedClass(Course.class).
buildSessionFactory();
		
	//create session
	Session session=factory.getCurrentSession();	
		
	try
	{

	session.beginTransaction();
	//get the id
	int theid=1;
	
	//get the instructor
	Instructor tempInstructor=session.get(Instructor.class,theid);
	
	System.out.println("Instructor "+tempInstructor);
	
	
	///CLOSE THE SESSION FOR A TEST FOR LAZY LOADING IF WE CLOSE THE SESSION LAZY WONT WORK
	session.close();
	
	System.out.println("Instructor Courses "+tempInstructor.getCourses()); //LAZY FETCH
	
	//commit transaction
	session.getTransaction().commit();

	
	System.out.println("Done !");
	}
	catch(Exception e)
	{
	e.printStackTrace();	
	}
	finally
	{
		factory.close();
	}
	
		
	}

}
