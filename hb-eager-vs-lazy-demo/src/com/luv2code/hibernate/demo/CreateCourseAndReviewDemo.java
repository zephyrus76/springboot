package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;

public class CreateCourseAndReviewDemo {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	//create Session Factory
SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").
addAnnotatedClass(Instructor.class).
addAnnotatedClass(InstructorDetail.class).
addAnnotatedClass(Course.class).
addAnnotatedClass(Review.class).buildSessionFactory();
		
	//create session
	Session session=factory.getCurrentSession();	
		
	try
	{

	session.beginTransaction();
	//get the id
	int theid=2;
	
	//get the instructor
	Course course=new Course("Spring Boot");
	
	Review t1=new Review("gud");
	Review t2=new Review("bad");
	
	course.addReview(t1);
	course.addReview(t2);
	
	session.save(course);
	
	
	session.getTransaction().commit();

	
	System.out.println("Done !");
	}
	catch(Exception e)
	{
	e.printStackTrace();	
	}
	finally
	{
		factory.close();
	}
	
		
	}

}
