package com.psm.spring_demo;

public class CricketCoach implements Coach {

	private FortuneService  fs=null;
	private String coachEmail=null;
	private String team=null;
	
	//seter metohod for inject dependency
	public void setFortuneService(FortuneService fs) {
		this.fs = fs;
	}
//for adding literal values
	
	public String getCoachEmail() {
		return coachEmail;
	}

	public void setCoachEmail(String coachEmail) {
		this.coachEmail = coachEmail;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "do batting everday";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return "bowling fortune is "+fs.fortuneService();
	}

}
