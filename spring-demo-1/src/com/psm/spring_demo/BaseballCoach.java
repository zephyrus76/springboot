package com.psm.spring_demo;

public class BaseballCoach implements Coach {

private FortuneService fs=null;

public BaseballCoach(FortuneService fss) {
	super();
	this.fs = fss;
}

@Override
public String getDailyWorkout() {
	// TODO Auto-generated method stub
	return "Spend 30 mins on batting practice";
}

@Override
public String getDailyFortune() {
	// TODO Auto-generated method stub
	//use my fs to get a fortune
	return fs.fortuneService();
}
}
