package com.psm.spring_demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeDemoApp {

	public static void main(String[] args) {
		//load the spring configuration file
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("beanScope-applicationContext2.xml");	
			
		//retrive bean from container 
		Coach theCoach=context.getBean("myCoach1",Coach.class);
	
	
		Coach alphaCoach=context.getBean("myCoach1",Coach.class);

		
//Singleton scope of the beans pointing to smae bean		
/*		System.out.println("reference of coach"+theCoach);

		System.out.println("reference of coach"+alphaCoach);
*/
		
		
//Prottype scope of the beans pointing to different beans		
		System.out.println("reference of coach"+theCoach);

		System.out.println("reference of coach"+alphaCoach);

	
	
	context.close();
	}
}
