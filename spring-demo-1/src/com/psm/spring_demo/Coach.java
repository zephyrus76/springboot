package com.psm.spring_demo;

public interface Coach {
 public String getDailyWorkout();

 public String getDailyFortune();

}
