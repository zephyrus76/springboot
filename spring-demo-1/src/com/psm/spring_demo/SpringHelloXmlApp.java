package com.psm.spring_demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringHelloXmlApp {
public static void main(String args[])
{
	//load the spring configuration file 
	ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
	//retrive the bean have fortune service dependency
//	Coach theCoach=context.getBean("myCoach",Coach.class);
//	
//	Coach theCoach1=context.getBean("myCoach1",Coach.class);
	
	CricketCoach theCoach2= context.getBean("myCoach2",CricketCoach.class);
	
	
	// calll methods on the bean
	
//	System.out.println("baseball "+theCoach.getDailyWorkout());
//
//	System.out.println("track "+theCoach1.getDailyWorkout());
//
//	
//	System.out.println("cricket "+theCoach2.getDailyWorkout());
//
//	//lets calll our new method for fortunes
//	System.out.println(theCoach.getDailyFortune());
//	System.out.println(theCoach1.getDailyFortune());
//	System.out.println(theCoach2.getDailyFortune());
//	

//get the literal values
	
	System.out.println("team is "+theCoach2.getTeam());
	System.out.println("email is "+theCoach2.getCoachEmail());
	
	
	//close the context
context.close();
}	
}
