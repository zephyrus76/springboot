package com.psm.spring_demo;

public class TrackCoach implements Coach {
 private FortuneService fs;
	public TrackCoach(FortuneService fs) {
	super();
	this.fs = fs;
}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Run a hard 5k";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return "Just Do it"+fs.fortuneService();
	}

	
	//add an init method
	public void doMyStartupStuff()
	{
		System.out.println("Trackcoach:::: inside stratup method");
	}
	
	//add an destroy method
		public void doMyDestroyStuff()
		{
			System.out.println("Trackcoach:::: inside shutdown method");
			
		}
		
	
}
